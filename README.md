# proxmox-users

Autocreates proxmox users from Gitlab accounts so that users logging in over OIDC get permissions without a manual step from an admin.
