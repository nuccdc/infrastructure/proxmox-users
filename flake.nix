{
  description = "proxmox-users";

  outputs = { self, nixpkgs }: let
    pkgs = import nixpkgs {
      system = "x86_64-linux";
    };
  in {
    devShell.x86_64-linux = pkgs.mkShell {
      name = "proxmox-users";
      buildInputs = with pkgs; [
        go
        gopls
      ];
    };
  };
}
