FROM golang:1.17-alpine AS build
WORKDIR /app
COPY go.mod ./
COPY go.sum ./
RUN go mod download
COPY *.go ./
RUN go build -o /proxmox-users

FROM alpine:latest
WORKDIR /
COPY --from=build /proxmox-users /proxmox-users
ENTRYPOINT ["/proxmox-users"]
