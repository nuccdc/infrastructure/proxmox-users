package main

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"os"

	gitlab "github.com/xanzy/go-gitlab"

	"github.com/sirupsen/logrus"
)

func init() {
	logrus.SetFormatter(&logrus.TextFormatter{
		FullTimestamp: true,
		PadLevelText:  true,
	})
}

func envOrFatal(envName string) string {
	val, wasSet := os.LookupEnv(envName)
	if !wasSet {
		logrus.Fatalf("environment variable missing: $%s", envName)
	}
	return val
}

var GITLAB_TOKEN string = envOrFatal("GITLAB_TOKEN")
var GITLAB_GROUP_ID string = envOrFatal("GITLAB_GROUP_ID")
var PROXMOX_URL string = envOrFatal("PROXMOX_URL")
var PROXMOX_TOKEN string = envOrFatal("PROXMOX_TOKEN")

func main() {
	gitlabClient, err := gitlab.NewClient(GITLAB_TOKEN)
	if err != nil {
		logrus.WithError(err).Errorln("could not initialize GitLab client")
		return
	}
	logrus.Info("GitLab client initialized.")

	// https://docs.gitlab.com/ee/api/members.html#list-all-members-of-a-group-or-project-including-inherited-members
	members, res, err := gitlabClient.Groups.ListAllGroupMembers(GITLAB_GROUP_ID, &gitlab.ListGroupMembersOptions{})
	if err != nil {
		logrus.WithError(err).WithField("groupId", GITLAB_GROUP_ID).Errorln("could not get group members from GitLab")
		return
	}

	logrus.WithFields(logrus.Fields{
		"status":     res.Status,
		"statusCode": res.StatusCode,
		"numMembers": len(members),
	}).Infoln("Got GitLab group members.")

	for _, member := range members {
		logrus.WithFields(logrus.Fields{
			"name":     member.Name,
			"username": member.Username,
		}).Infoln("Handling member.")
		proxmoxCreateMember(member)
	}
}

type ReqBody struct {
	UserID    string `json:"userid"`
	Firstname string `json:"firstname"`
	Comment   string `json:"comment"`
	Groups    string `json:"groups"`
}

// https://pve.proxmox.com/pve-docs/api-viewer/index.html#/access/users
func proxmoxCreateMember(member *gitlab.GroupMember) {
	url := PROXMOX_URL + "/api2/json/access/users" // TODO
	body := ReqBody{
		UserID:    member.Username + "@auth0",
		Firstname: member.Name,
		Comment:   "Created automatically",
		Groups:    "team",
	}
	bodyBytes, err := json.Marshal(body)
	if err != nil {
		logrus.WithError(err).Errorln("could not marshal request body to JSON")
		return
	}

	httpReq, err := http.NewRequest("POST", url, bytes.NewReader(bodyBytes))
	if err != nil {
		logrus.WithError(err).Errorln("error setting up HTTP request")
		return
	}

	httpReq.Header.Add("Authorization", "PVEAPIToken="+PROXMOX_TOKEN)
	httpReq.Header.Add("Content-Type", "application/json")
	res, err := http.DefaultClient.Do(httpReq)
	if err != nil {
		logrus.WithError(err).Errorln("error sending HTTP request to Proxmox API")
		return
	}

	if res.StatusCode != 200 {
		byts, _ := io.ReadAll(res.Body)
		bodyText := string(byts)
		logrus.WithFields(logrus.Fields{
			"statusCode": res.StatusCode,
			"body":       bodyText,
		}).Errorln("Proxmox API error")
		return
	}

	logrus.WithFields(logrus.Fields{
		"statusCode": res.StatusCode,
		"name":       member.Name,
		"username":   member.Username,
	}).Infoln("successfully created Proxmox user")
}
